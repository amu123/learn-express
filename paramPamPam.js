var express = require('express');
var app = express();

app.put('/message/:id', function(req, res, next){
	var id = req.params.id;
	
	var sha1 = require('crypto')
      .createHash('sha1')
      .update(new Date().toDateString() + id)
      .digest('hex');


      res.send(sha1);
})
app.listen(process.argv[2]);