var express = require('express');
var fs = require('fs');
var app = express();

app.get('/books', function(req, res){
	var filename = process.argv[3];

	fs.readFile(filename, function(err, data){

		if(err){
			throw err;
		}
		else{
			books = JSON.parse(data);
		}
		res.json(books);

	})
})
app.listen(process.argv[2]);